/******************************************************************************

LilyPad Pixel Board - Set All Pixels
Angela Sheehan
SparkFun Electronics

Adapted from SparkFun's WS2812 Breakout Hookup Guide code examples

This code demonstrates setting all LilyPad Pixel Boards in the project to one
color using the NeoPixel library.

******************************************************************************/

#include <Adafruit_NeoPixel.h>

#define PIN 6  //Which pin the pixels are connected to
#define LED_COUNT 3  //Number of pixels used

// Create an instance of the Adafruit_NeoPixel class called "leds".
// That'll be what we refer to from here on...
Adafruit_NeoPixel leds = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);

void setup()
{
 leds.begin();  // Start up the LED strip.
 leds.show();   // LEDs don't actually update until you call this.
}

void loop()
{
  //Use a for loop to scroll through each pixel
  for(int x=0; x<LED_COUNT; x++)
  {
    //Set the pixel to YELLOW
    leds.setPixelColor(x, 255, 255, 0); 
  }
  leds.show(); //Display the color

}
